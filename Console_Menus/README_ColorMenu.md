[[_TOC_]]

# ColorMenu

This is an extremely customizable menu with optional color support. 

## Format of a Menu

```text
Franklin's Pet Shop      <-- title (optional)
-------------------      <-- underline (optional)

Our current animals      <-- pre_menu_text (optional)

  1) Cat                 <-- item at index 0
  2) Dog                 <-- index_separator = ) (optional)
  3) Hamster

All animals vaccinated   <-- post_menu_text (optional)

Choose An Animal: 3      <-- prompt (optional)
                         <-- prompt_separator = : (optional)
```

## Class Parameters / Properties

| Parameter / Property | Default | Description |
| --- | --- | --- |
| items | n/a | **REQUIRED:** This is an iterable with the menu items to be given as a choice to the user. |
| title | Menu | The title of the menu. |
| prompt | Enter Choice | The text of the user prompt. |
| pre_menu_text | n/a | This is text that will print after the menu title but before the list of menu items. |
| post_menu_text | n/a | This is text that will print after the menu items but before the user prompt. |
| underline_chars | - | This is the character(s) that make up the underline of the menu title. |
| index_separator | ) | This is the character(s) used after the menu index number and before the menu item. |
| prompt_separator | : | This is the character(s) used after the user prompt text and before the cursor where the user enters his/her selection. |
| color_enabled | True | True / False: If color should be utilized (see Themes below) |
| reprint_on_error | False | True / False: When a user enters an invalid choice, they get a notice and the prompt is reprinted.  If this is set to True, the entire menu will be reprinted on an error. |

## Other Class Properties

| Property | Default | Description |
| --- | --- | --- |
| index | None | This holds the last selected index from the user selection. |
| item | None | This holds the last selected item text from the user selection. |
| theme | See Themes below | This holds the color theme for each part of the menu. |

## Functions

| Function | Parameter | Default | Description |
| --- | --- | --- | --- |
| choose() | n/a | n/a | This prints the menu and prompts the user to select an item. |

## Theme

Theme properties are set as a tuple or list of 2 items, a foreground and/or background ANSI color index in the range of 0 - 15.

| Property | Description |
| --- | --- |
| default_color | This is the color code that will be used on any non-menu items, such as the end of each line, and also the very last code sent to the terminal before quitting. |
| error_color | This is the color used when the user makes an invalid choice. |
| title_color | Color used for the title of the menu. |
| underline_color | Color used for the underline of the menu title. |
| pre_menu_text_color | Color used for the text between the menu title and the menu items. |
| index_color | Color used for the number (index) of the menu items. |
| index_separator_color | Color used for the index separator character(s). |
| item_color | Color used for the menu item text. |
| post_menu_text_color | Color used for the text after the menu items and before the user prompt. |
| prompt_color | Color used for the user prompt text. |
| prompt_separator_color | Color used for the character(s) after the prompt text. |
| entry_color | Color the cursor will be left at for the user to enter his/her choice. |
