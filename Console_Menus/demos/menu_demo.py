import sys
sys.path.append('../src')
sys.path.append('../../ANSI_Codes/src')
from color16 import color
from ColorMenu import ColorMenu

# items = ["Cat", "Dog", "Fish", "Parrot", "Ferret", "Hamster",
#          "Guinea Pig", "Snake", "Chameleon", "Rabbit", "Parakeet", "Cow"]
items = ["Cat", "Dog", "Hamster"]
title = "Franklin's Pet Shop"
pre_text = "Our current animals"
post_text = "All animals vaccinated"
prompt = "Choose An Animal"
underline_chars = "-"
index_separator = ")"
prompt_separator = ":"
reprint_on_error = True

m = ColorMenu(items, title=title, prompt=prompt, pre_menu_text=pre_text, post_menu_text=post_text,
              reprint_on_error=reprint_on_error, underline_chars=underline_chars,
              index_separator=index_separator, prompt_separator=prompt_separator)
index, item = m.choose()
print(f"You chose index {index}, item {item}")
# print(m.theme)
