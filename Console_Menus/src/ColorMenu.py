class ColorMenu:

    @staticmethod
    # source: https://gitlab.com/joeysbytes/python-snippets/-/raw/master/ANSI_Codes/src/color16.py
    def _color(fg=None, bg=None):
        color_codes = [[30, 31, 32, 33, 34, 35, 36, 37, 90, 91, 92, 93, 94, 95, 96, 97],
                       [40, 41, 42, 43, 44, 45, 46, 47, 100, 101, 102, 103, 104, 105, 106, 107]]
        ansi_code, csi, color_end = "", '\033' + '[', 'm'
        if fg is not None: ansi_code += f"{csi}{color_codes[0][fg]}{color_end}"
        if bg is not None: ansi_code += f"{csi}{color_codes[1][bg]}{color_end}"
        return ansi_code

    def __init__(self, items, title="Menu", prompt="Enter Choice", pre_menu_text=None, post_menu_text=None,
                 underline_chars="-", index_separator=")", prompt_separator=":", color_enabled=True,
                 reprint_on_error=False):
        self.index, self.item, self.items = None, None, [str(i) for i in items]
        self.title, self.reprint_on_error = None if title is None else str(title), bool(reprint_on_error)
        self.prompt, self.color_enabled = None if prompt is None else str(prompt), bool(color_enabled)
        self.pre_menu_text = None if pre_menu_text is None else str(pre_menu_text)
        self.post_menu_text = None if post_menu_text is None else str(post_menu_text)
        self.underline_chars = None if underline_chars is None else str(underline_chars)
        self.index_separator = None if index_separator is None else str(index_separator)
        self.prompt_separator = None if prompt_separator is None else str(prompt_separator)
        self.theme = {"default_color": (7, 0), "error_color": (11, 1), "title_color": (6,), "index_color": (7,),
                      "underline_color": (6,), "pre_menu_text_color": (2,), "index_separator_color": (6,),
                      "item_color": (7,), "post_menu_text_color": (2,), "prompt_separator_color": (6,),
                      "prompt_color": (6,), "entry_color": (7,)}
        if len(items) == 0: raise ValueError("No items provided for menu.")

    def _get_color(self, theme_property):
        return ColorMenu._color(*self.theme[theme_property]) if self.color_enabled else ""

    def _get_newline(self, lines=1):
        return self._get_color("default_color") + "\n" * lines

    def _build_menu(self):
        menu_text = self._get_color("default_color")
        # Menu Title
        if (self.title is not None) and (len(self.title) > 0):
            menu_text += self._get_color("title_color") + self.title + self._get_newline()
            # Menu title underline
            if (self.underline_chars is not None) and (len(self.underline_chars) > 0):
                menu_text += self._get_color("underline_color")
                menu_text += (self.underline_chars * len(self.title))[0:len(self.title)] + self._get_newline()
            menu_text += "\n"
        # Pre-Menu Text
        if (self.pre_menu_text is not None) and (len(self.pre_menu_text) > 0):
            menu_text += self._get_color("pre_menu_text_color") + self.pre_menu_text + self._get_newline(2)
        # Menu Items
        for idx, item in enumerate(self.items):
            menu_text += self._get_color("index_color") + f"{idx + 1:>3}" + self._get_color("index_separator_color")
            menu_text += self.index_separator + " " + self._get_color("item_color") + item + self._get_newline()
        menu_text += self._get_newline()
        # Post-Menu Text
        if (self.post_menu_text is not None) and (len(self.post_menu_text) > 0):
            menu_text += self._get_color("post_menu_text_color") + self.post_menu_text + self._get_newline(2)
        return menu_text

    def _build_prompt(self):
        prompt_text = self._get_color("prompt_color") + self.prompt + self._get_color("prompt_separator_color")
        prompt_text += self.prompt_separator + " " + self._get_color("entry_color")
        return prompt_text

    def _build_invalid_choice(self):
        return self._get_color("error_color") + "*** INVALID CHOICE ***"

    def choose(self):
        menu_text, prompt_text, invalid_text = self._build_menu(), self._build_prompt(), self._build_invalid_choice()
        print(menu_text, end="")
        while True:
            try:
                choice = int(input(prompt_text))
                if 1 <= choice <= len(self.items):
                    self.index, self.item = choice - 1, self.items[choice - 1]
                    print(self._get_newline(), end=""); return self.index, self.item
                raise ValueError
            except (ValueError, TypeError):
                print(self._get_newline() + invalid_text + self._get_newline())
                if self.reprint_on_error: print(menu_text, end="")
