# Tiny Menu

## Description

Given a list of items, this will present the user with the list, and ask them to enter a number corresponding
to an item. This method returns the index value of the list that the user chose.

## Parameters

* items - Sequence type, of the menu items to display
* title - String, the title of the menu (optional)
* prompt - String, the user prompt text (optional)

## To-Do

* Unit Tests - Manual testing has been fully done.
