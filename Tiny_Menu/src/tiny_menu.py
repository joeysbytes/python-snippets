def menu(items: Sequence, title: Optional[str]=None, prompt: Optional[str]="Enter Choice: ") -> int:
    if (isinstance(items, Sequence)) and (len(items) > 0):
        while True:
            print("\n" if title is None else f"\n{title}\n\n", end="")
            for idx, item in enumerate(items): print(f"{idx+1: >2}) {item}")
            try:
                menu_idx = int(input("\n> " if prompt is None else f"\n{prompt}")) - 1
                if not 0 <= menu_idx < len(items):
                    raise ValueError
                print(); return menu_idx
            except Exception:
                print("\nERROR: Invalid Choice\n")
    else:
        raise ValueError(f"ERROR: Invalid sequence of menu items: {items}")
