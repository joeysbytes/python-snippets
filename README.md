# Python Snippets

These are small functions / methods / classes which are meant to be copied in to your projects, instead of installed like an external library.

Each "snippet" will have its own README.md to explain its usage.

Unless otherwise noted, the minimum Python version for the snippets is 3.6, and no external dependencies outside the standard Python library are needed.
