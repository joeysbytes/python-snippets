# Tiny Yes/No

Given a prompt question (remember to include the question mark), prompt the user and get a
yes/no response. Only the first character is examined, and case does not matter.
