def yes_no(prompt: str) -> bool:
    while True:
        try:
            answer = input(f"{prompt} (y/n): ")[0:1].lower()
            if (answer != "y") and (answer != "n"):
                raise ValueError()
            return True if answer == "y" else False
        except Exception:
            print("\nInvalid Answer: Please type 'y' or 'n'\n")
