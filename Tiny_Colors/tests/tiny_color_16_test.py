import pytest
from ..src.tiny_color_16 import color16


@pytest.mark.parametrize(
    "fg_index, expected_fg_code",
    [(0, 30), (1, 31), (2, 32), (3, 33), (4, 34), (5, 35), (6, 36), (7, 37),
     (8, 90), (9, 91), (10, 92), (11, 93), (12, 94), (13, 95), (14, 96), (15, 97),
     (-1, 39), (16, 39), (100, 39)
    ])
def test_color16_valid_foreground_indexes(fg_index, expected_fg_code):
    expected = '\033' + "[" + str(expected_fg_code) + "m"
    actual = color16(fg=fg_index)
    assert actual == expected


@pytest.mark.parametrize(
    "bg_index, expected_bg_code",
    [(0, 40), (1, 41), (2, 42), (3, 43), (4, 44), (5, 45), (6, 46), (7, 47),
     (8, 100), (9, 101), (10, 102), (11, 103), (12, 104), (13, 105), (14, 106), (15, 107),
     (-1, 49), (16, 49), (100, 49)
    ])
def test_color16_valid_background_indexes(bg_index, expected_bg_code):
    expected = '\033' + "[" + str(expected_bg_code) + "m"
    actual = color16(bg=bg_index)
    assert actual == expected


INVALID_INDEXES = ["a", 3.1, None]


@pytest.mark.parametrize("fg_index", INVALID_INDEXES)
def test_color16_invalid_foreground_indexes(fg_index):
    expected = ""
    actual = color16(fg=fg_index)
    assert actual == expected


@pytest.mark.parametrize("bg_index", INVALID_INDEXES)
def test_color16_invalid_background_indexes(bg_index):
    expected = ""
    actual = color16(bg=bg_index)
    assert actual == expected


def test_color16_foreground_plus_background():
    fg_index = 3
    bg_index = 8
    expected = '\033' + "[33m" + '\033' + "[100m"
    actual = color16(fg_index, bg_index)
    assert actual == expected
