import pytest
from ..src.tiny_color_256 import color256

@pytest.mark.parametrize(
    "fg_index, expected_fg_code",
    [(4, 4), (16, 16), (32, 32), (64, 64), (128, 128), (192, 192), (255, 255),
     (-1, 39), (256, 39), (1000, 39)
    ])
def test_color256_valid_foreground_indexes(fg_index, expected_fg_code):
    expected = '\033' + "[38;5;" + str(expected_fg_code) + "m"
    actual = color256(fg=fg_index)
    assert actual == expected


@pytest.mark.parametrize(
    "bg_index, expected_bg_code",
    [(4, 4), (16, 16), (32, 32), (64, 64), (128, 128), (192, 192), (255, 255),
     (-1, 49), (256, 49), (1000, 49)
    ])
def test_color256_valid_background_indexes(bg_index, expected_bg_code):
    expected = '\033' + "[48;5;" + str(expected_bg_code) + "m"
    actual = color256(bg=bg_index)
    assert actual == expected


INVALID_INDEXES = ["a", 3.1, None]


@pytest.mark.parametrize("fg_index", INVALID_INDEXES)
def test_color256_invalid_foreground_indexes(fg_index):
    expected = ""
    actual = color256(fg=fg_index)
    assert actual == expected


@pytest.mark.parametrize("bg_index", INVALID_INDEXES)
def test_color256_invalid_background_indexes(bg_index):
    expected = ""
    actual = color256(bg=bg_index)
    assert actual == expected


def test_color256_foreground_plus_background():
    fg_index = 32
    bg_index = 192
    expected = '\033' + "[38;5;32m" + '\033' + "[48;5;192m"
    actual = color256(fg_index, bg_index)
    assert actual == expected
