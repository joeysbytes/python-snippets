def color256(fg: int=None,  bg: int=None) -> str:
    code = ""
    if (fg is not None) and (isinstance(fg, int)):
        fg_code = fg if 0 <= fg <= 255 else 39; code += f"\033[38;5;{fg_code}m"
    if (bg is not None) and (isinstance(bg, int)):
        bg_code = bg if 0 <= bg <= 255 else 49; code += f"\033[48;5;{bg_code}m"
    return code
