def color16(fg: int=None,  bg: int=None) -> str:
    code = ""
    if (fg is not None) and (isinstance(fg, int)):
        fg_code = 30 + fg if 0 <= fg <= 7 else 82 + fg if 8 <= fg <= 15 else 39; code += f"\033[{fg_code}m"
    if (bg is not None) and (isinstance(bg, int)):
        bg_code = 40 + bg if 0 <= bg <= 7 else 92 + bg if 8 <= bg <= 15 else 49; code += f"\033[{bg_code}m"
    return code
