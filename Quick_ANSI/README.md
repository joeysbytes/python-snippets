[[_TOC_]]

# Quick ANSI

## Description

Quick ANSI is a single module which gives access to commonly used colors, attributes,
and other codes for your terminal programs.

<hr />

## Core Codes

| Constant | Description |
| --- | --- |
| RESET | Resets all ANSI colors, attributes, etc. to terminal defaults. |
| CLEAR<br >CLEAR_SCREEN | Clears the screen and moves the cursor to the top left corner. |
| DEFAULT_COLOR<br />DEFAULT_COLOR_FG | Sets the foreground color to the terminal default. |
| DEFAULT_COLOR_BG | Sets the background color to the terminal default. |
| DEFAULT_COLORS | Sets both the foreground and background colors to the terminal default. |


<hr />

## Attributes

| Constant | Description |
| --- | --- |
| DIM<br />NORMAL<br />BOLD | Sets the intensity to either dim, normal, or bold. |
| BLINK<br />BLINK_ON | Turns blinking text on. |
| BLINK_OFF | Turns blinking text off. |
| ITALIC<br />ITALIC_ON | Turns italic text on. |
| ITALIC_OFF | Turns italic text off. |
| REVERSE<br />REVERSE_ON | Turns reverse text on. |
| REVERSE_OFF | Turns reverse text off. |
| STRIKETHROUGH<br />STRIKETHROUGH_ON | Turns strikethrough text on. |
| STRIKETHROUGH_OFF | Turns strikethrough text off. |
| UNDERLINE<br />UNDERLINE_ON | Turns underline text on. |
| UNDERLINE_OFF | Turns underline text off. |
| HIDE<br />HIDE_ON<br />SHOW_OFF | Hide text. |
| SHOW<br />SHOW_ON<br />HIDE_OFF | Show (unhide) text. |

<hr />

## Colors

### 16-Color Palette Functions

The color(), color16() and c16() functions are all the same function. It requires at least one of the following parameters:

1. fg: foreground index 0 - 15, optional
1. bg: background index 0 - 15, optional

### 16-Color Palette Constants

| Index | Foreground Constant | Background Constant | Index | Foreground Constant | Background Constant |
| --- | --- | --- | --- | --- | --- |
| 0 | BLACK<br />BLACK_FG | BLACK_BG | 8 | BRIGHT_BLACK<br />BRIGHT_BLACK_FG | BRIGHT_BLACK_BG |
| 1 | RED<br />RED_FG | RED_BG | 9 | BRIGHT_RED<br />BRIGHT_RED_FG | BRIGHT_RED_BG |
| 2 | GREEN<br />GREEN_FG | GREEN_BG | 10 | BRIGHT_GREEN<br />BRIGHT_GREEN_FG | BRIGHT_GREEN_BG |
| 3 | YELLOW<br />YELLOW_FG | YELLOW_BG | 11 | BRIGHT_YELLOW<br />BRIGHT_YELLOW_FG | BRIGHT_YELLOW_BG |
| 4 | BLUE<br />BLUE_FG | BLUE_BG | 12 | BRIGHT_BLUE<br />BRIGHT_BLUE_FG | BRIGHT_BLUE_BG |
| 5 | MAGENTA<br />MAGENTA_FG | MAGENTA_BG | 13 | BRIGHT_MAGENTA<br />BRIGHT_MAGENTA_FG | BRIGHT_MAGENTA_BG |
| 6 | CYAN<br />CYAN_FG | CYAN_BG | 14 | BRIGHT_CYAN<br />BRIGHT_CYAN_FG | BRIGHT_CYAN_BG |
| 7 | WHITE<br />WHITE_FG | WHITE_BG | 15 | BRIGHT_WHITE<br />BRIGHT_WHITE_FG | BRIGHT_WHITE_BG |

<hr />

### 256-Color Palette Functions

The color256() and c256() functions are the same function. It requires at least one of the following parameters:

1. fg: foreground index 0 - 255, optional
1. bg: background index 0 - 255, optional

### 256-Color Palette Break-Out

| Color Range | Description |
| --- | --- |
| 0 - 15 | Same as the 16-color palette. |
| 16 - 231 | [The 216-color 6 x 6 x 6 color cube.](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit) |
| 232 - 255 | 24 grays going from dark to light. |

<hr />

### RGB-Color Pallete Functions

The rgb() function requires at least one of the following parameters:

1. fg: A sequence (list, tuple, etc) of 3 integers in the range 0 - 255, optional
1. bg: A sequence (list, tuple, etc) of 3 integers in the range 0 - 255, optional

The function hex_to_rgb() is a utility that will convert a hex string to a 3 item sequence, which can
then be used in the rgb() function.  The hex code string has the following format rules:

* Can optionally start with a # sign.
* Must be 6 characters long (not counting the # sign)
* Can be any mix of 0-9, a-f, A-F characters.
