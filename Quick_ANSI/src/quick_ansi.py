from typing import Optional, Sequence, Tuple
import re


###########################################################################
# Core Constants
###########################################################################

_ESC = '\033'
_CSI = f"{_ESC}["


###########################################################################
# Screen Constants
###########################################################################

# Sets all colors / attributes back to terminal defaults
RESET = f"{_CSI}0m"

# Clear the screen and move the cursor to the top left corner
CLEAR = f"{_CSI}2J{_CSI}1;1H"
CLEAR_SCREEN = CLEAR

# Set back to terminal-defined default colors
DEFAULT_COLOR = f"{_CSI}39m"
DEFAULT_COLOR_FG = DEFAULT_COLOR
DEFAULT_COLOR_BG = f"{_CSI}49m"
DEFAULT_COLORS = f"{DEFAULT_COLOR_FG}{DEFAULT_COLOR_BG}"


###########################################################################
# Attributes
###########################################################################

NORMAL = f"{_CSI}22m"
BOLD = f"{NORMAL}{_CSI}1m"
DIM = f"{NORMAL}{_CSI}2m"

ITALIC = f"{_CSI}3m"
ITALIC_ON = ITALIC
ITALIC_OFF = f"{_CSI}23m"

UNDERLINE = f"{_CSI}4m"
UNDERLINE_ON = UNDERLINE
UNDERLINE_OFF = f"{_CSI}24m"

BLINK = f"{_CSI}5m"
BLINK_ON = BLINK
BLINK_OFF = f"{_CSI}25m"

REVERSE = f"{_CSI}7m"
REVERSE_ON = REVERSE
REVERSE_OFF = f"{_CSI}27m"

STRIKETHROUGH = f"{_CSI}9m"
STRIKETHROUGH_ON = STRIKETHROUGH
STRIKETHROUGH_OFF = f"{_CSI}29m"

HIDE = f"{_CSI}8m"
SHOW = f"{_CSI}28m"
HIDE_ON = HIDE
HIDE_OFF = SHOW
SHOW_ON = SHOW
SHOW_OFF = HIDE


###########################################################################
# 16 Colors
###########################################################################

_FG_16_VALUES = (30, 31, 32, 33, 34, 35, 36, 37, 90, 91, 92, 93, 94, 95, 96, 97)
_BG_16_VALUES = (40, 41, 42, 43, 44, 45, 46, 47, 100, 101, 102, 103, 104, 105, 106, 107)


def color16(fg: Optional[int] = None, bg: Optional[int] = None) -> str:
    """Return the ANSI 16-color code for a foreground and/or background value."""
    if (fg is None) and (bg is None):
        raise ValueError("No 16-Color Values Provided")
    color_code = ""
    # foreground color
    if fg is not None:
        if (isinstance(fg, int)) and (0 <= fg <= 15):
            color_code += f"{_CSI}{_FG_16_VALUES[fg]}m"
        else:
            raise ValueError(f"Invalid 16-Color Foreground Value [0 - 15]: {fg}")
    # background color
    if bg is not None:
        if (isinstance(bg, int)) and (0 <= bg <= 15):
            color_code += f"{_CSI}{_BG_16_VALUES[bg]}m"
        else:
            raise ValueError(f"Invalid 16-Color Background Value [0 - 15]: {bg}")
    return color_code


# Aliases for color16
c16 = color16
color = color16


# 16-Color Constants: Foreground
BLACK = color16(fg=0)
RED = color16(fg=1)
GREEN = color16(fg=2)
YELLOW = color16(fg=3)
BLUE = color16(fg=4)
MAGENTA = color16(fg=5)
CYAN = color16(fg=6)
WHITE = color16(fg=7)
BRIGHT_BLACK = color16(fg=8)
BRIGHT_RED = color16(fg=9)
BRIGHT_GREEN = color16(fg=10)
BRIGHT_YELLOW = color16(fg=11)
BRIGHT_BLUE = color16(fg=12)
BRIGHT_MAGENTA = color16(fg=13)
BRIGHT_CYAN = color16(fg=14)
BRIGHT_WHITE = color16(fg=15)

# 16-Color Constants: Foreground_FG
BLACK_FG = BLACK
RED_FG = RED
GREEN_FG = GREEN
YELLOW_FG = YELLOW
BLUE_FG = BLUE
MAGENTA_FG = MAGENTA
CYAN_FG = CYAN
WHITE_FG = WHITE
BRIGHT_BLACK_FG = BRIGHT_BLACK
BRIGHT_RED_FG = BRIGHT_RED
BRIGHT_GREEN_FG = BRIGHT_GREEN
BRIGHT_YELLOW_FG = BRIGHT_YELLOW
BRIGHT_BLUE_FG = BRIGHT_BLUE
BRIGHT_MAGENTA_FG = BRIGHT_MAGENTA
BRIGHT_CYAN_FG = BRIGHT_CYAN
BRIGHT_WHITE_FG = BRIGHT_WHITE

# 16-Color Constants: Background
BLACK_BG = color16(bg=0)
RED_BG = color16(bg=1)
GREEN_BG = color16(bg=2)
YELLOW_BG = color16(bg=3)
BLUE_BG = color16(bg=4)
MAGENTA_BG = color16(bg=5)
CYAN_BG = color16(bg=6)
WHITE_BG = color16(bg=7)
BRIGHT_BLACK_BG = color16(bg=8)
BRIGHT_RED_BG = color16(bg=9)
BRIGHT_GREEN_BG = color16(bg=10)
BRIGHT_YELLOW_BG = color16(bg=11)
BRIGHT_BLUE_BG = color16(bg=12)
BRIGHT_MAGENTA_BG = color16(bg=13)
BRIGHT_CYAN_BG = color16(bg=14)
BRIGHT_WHITE_BG = color16(bg=15)


###########################################################################
# 256 Colors
###########################################################################

def color256(fg: Optional[int] = None, bg: Optional[int] = None) -> str:
    """Return the ANSI 256-color code for a foreground and/or background value."""
    if (fg is None) and (bg is None):
        raise ValueError("No 256-Color Values Provided")
    color_code = ""
    # foreground color
    if fg is not None:
        if (isinstance(fg, int)) and (0 <= fg <= 255):
            color_code += f"{_CSI}38;5;{fg}m"
        else:
            raise ValueError(f"Invalid 256-Color Foreground Value [0 - 255]: {fg}")
    # background color
    if bg is not None:
        if (isinstance(bg, int)) and (0 <= bg <= 255):
            color_code += f"{_CSI}48;5;{bg}m"
        else:
            raise ValueError(f"Invalid 256-Color Background Value [0 - 255]: {bg}")
    return color_code


# Aliases for color256
c256 = color256


###########################################################################
# RGB Colors
###########################################################################

def rgb(fg: Optional[Sequence[int]] = None,
        bg: Optional[Sequence[int]] = None) -> str:
    """Return the ANSI RGB-color code for a foreground and/or background sequence."""
    if (fg is None) and (bg is None):
        raise ValueError("No RGB-Color Values Provided")
    color_code = ""
    # foreground color
    if fg is not None:
        if ((isinstance(fg, Sequence)) and (len(fg) == 3) and
                (isinstance(fg[0], int)) and (0 <= fg[0] <= 255) and
                (isinstance(fg[1], int)) and (0 <= fg[1] <= 255) and
                (isinstance(fg[2], int)) and (0 <= fg[2] <= 255)):
            color_code += f"{_CSI}38;2;{fg[0]};{fg[1]};{fg[2]}m"
        else:
            raise ValueError(f"Invalid RGB-Color Foreground Value(s) [0 - 255]: {fg}")
    # background color
    if bg is not None:
        if ((isinstance(bg, Sequence)) and (len(bg) == 3) and
                (isinstance(bg[0], int)) and (0 <= bg[0] <= 255) and
                (isinstance(bg[1], int)) and (0 <= bg[1] <= 255) and
                (isinstance(bg[2], int)) and (0 <= bg[2] <= 255)):
            color_code += f"{_CSI}48;2;{bg[0]};{bg[1]};{bg[2]}m"
        else:
            raise ValueError(f"Invalid RGB-Color Background Value(s) [0 - 255]: {bg}")
    return color_code


_HEX_TO_RGB_REGEX = r"^#?[0-9A-Fa-f]{6}$"


def hex_to_rgb(hex_code: str) -> Tuple[int, int, int]:
    """Return the RGB sequence of a given hex code."""
    if (isinstance(hex_code, str)) and (re.match(_HEX_TO_RGB_REGEX, hex_code)):
        hex_vl = hex_code.replace("#", "")
        red = int(hex_vl[0:2], 16)
        green = int(hex_vl[2:4], 16)
        blue = int(hex_vl[4:6], 16)
        return (red, green, blue)
    else:
        raise ValueError(f"Invalid 6-character hex value: {hex_code}")
