import pytest
from ..src.quick_ansi import *


###########################################################################
# RGB Colors
###########################################################################

def test_rgb_no_values_provided():
    expected = "No RGB-Color Values Provided"
    with pytest.raises(ValueError) as ve:
        rgb()
    assert expected == str(ve.value)


RGB_INVALID_VALUES = [
    (-1, 0, 0), (256, 0, 0), ("a", 0, 0), (3.5, 0, 0), (None, 0, 0),
    (0, -1, 0), (0, 256, 0), (0, "a", 0), (0, 3.5, 0), (0, None, 0),
    (0, 0, -1), (0, 0, 256), (0, 0, "a"), (0, 0, 3.5), (0, 0, None),
    (), (0, ), (0, 0), (0, 0, 0, 0),
    "a", 3.5, 0
    ]


@pytest.mark.parametrize("fg_seq", RGB_INVALID_VALUES)
def test_rgb_invalid_foreground_values(fg_seq):
    expected = f"Invalid RGB-Color Foreground Value(s) [0 - 255]: {fg_seq}"
    with pytest.raises(ValueError) as ve:
        rgb(fg=fg_seq)
    assert expected == str(ve.value)


@pytest.mark.parametrize("bg_seq", RGB_INVALID_VALUES)
def test_rgb_invalid_background_values(bg_seq):
    expected = f"Invalid RGB-Color Background Value(s) [0 - 255]: {bg_seq}"
    with pytest.raises(ValueError) as ve:
        rgb(bg=bg_seq)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "red, green, blue",
    [(16, 32, 64), [128, 255, 4]
    ])
def test_rgb_valid_foreground_values(red, green, blue):
    expected = '\033' + f"[38;2;{red};{green};{blue}m"
    actual = rgb(fg=(red, green, blue))
    assert actual == expected


@pytest.mark.parametrize(
    "red, green, blue",
    [(16, 32, 64), [128, 255, 4]
    ])
def test_rgb_valid_background_values(red, green, blue):
    expected = '\033' + f"[48;2;{red};{green};{blue}m"
    actual = rgb(bg=(red, green, blue))
    assert actual == expected


def test_rgb_foreground_plus_background():
    fg_rgb = (16, 32, 64)
    bg_rgb = (128, 255, 4)
    expected = rgb(fg=fg_rgb)
    expected += rgb(bg=bg_rgb)
    actual = rgb(fg=fg_rgb, bg=(bg_rgb))
    assert actual == expected


###########################################################################
# Hex to RGB Converter
###########################################################################

@pytest.mark.parametrize(
    "hex_code",
    [None, 5, 3.4,
     "#", "#1", "#12", "#123", "#1234", "#12345", "#1234567",
     "", "1", "12", "123", "1234", "12345", "1234567"
    ])
def test_hex_to_rgb_invalid_values(hex_code):
    expected = f"Invalid 6-character hex value: {hex_code}"
    with pytest.raises(ValueError) as ve:
        hex_to_rgb(hex_code)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "hex_code, rgb_seq",
    [("123456", (18, 52, 86)),
     ("#abcdef", (171, 205, 239)),
     ("#987654", (152, 118, 84)),
     ("FEDCBA", (254, 220, 186)),
     ("00fF7a", (0, 255, 122))
    ])
def test_hex_to_rgb_valid_values(hex_code, rgb_seq):
    actual = hex_to_rgb(hex_code)
    assert actual == rgb_seq
