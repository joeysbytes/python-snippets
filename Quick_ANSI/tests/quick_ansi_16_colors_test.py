import pytest
from ..src.quick_ansi import *


def test_color16_no_values_provided():
    expected = "No 16-Color Values Provided"
    with pytest.raises(ValueError) as ve:
        color16()
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "fg_index",
     ["a", -1, 16, 3.5])
def test_color16_invalid_foreground_values(fg_index):
    expected = f"Invalid 16-Color Foreground Value [0 - 15]: {fg_index}"
    with pytest.raises(ValueError) as ve:
        color16(fg=fg_index)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "bg_index",
     ["a", -1, 16, 3.5])
def test_color16_invalid_background_values(bg_index):
    expected = f"Invalid 16-Color Background Value [0 - 15]: {bg_index}"
    with pytest.raises(ValueError) as ve:
        color16(bg=bg_index)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "color_index, color_value",
    [(0, 30), (1, 31), (2, 32), (3, 33), (4, 34), (5, 35), (6, 36), (7, 37),
     (8, 90), (9, 91), (10, 92), (11, 93), (12, 94), (13, 95), (14, 96), (15, 97)
    ])
def test_color16_foreground_colors(color_index, color_value):
    expected = '\033' + "[" + str(color_value) + "m"
    actual = color16(fg=color_index)
    assert actual == expected


@pytest.mark.parametrize(
    "color_index, color_value",
    [(0, 40), (1, 41), (2, 42), (3, 43), (4, 44), (5, 45), (6, 46), (7, 47),
     (8, 100), (9, 101), (10, 102), (11, 103), (12, 104), (13, 105), (14, 106), (15, 107)
    ])
def test_color16_background_colors(color_index, color_value):
    expected = '\033' + "[" + str(color_value) + "m"
    actual = color16(bg=color_index)
    assert actual == expected


def test_color16_foreground_plus_background():
    fg_index = 5
    bg_index = 14
    expected = color16(fg=fg_index)
    expected += color16(bg=bg_index)
    actual = color16(fg_index, bg_index)
    assert actual == expected


@pytest.mark.parametrize(
    "fg_index, constant",
    [(0, BLACK), (1, RED), (2, GREEN), (3, YELLOW),
     (4, BLUE), (5, MAGENTA), (6, CYAN), (7, WHITE),
     (8, BRIGHT_BLACK), (9, BRIGHT_RED), (10, BRIGHT_GREEN), (11, BRIGHT_YELLOW),
     (12, BRIGHT_BLUE), (13, BRIGHT_MAGENTA), (14, BRIGHT_CYAN), (15, BRIGHT_WHITE)
    ])
def test_color16_foreground_constants(fg_index, constant):
    expected = color16(fg=fg_index)
    assert constant == expected


@pytest.mark.parametrize(
    "bg_index, constant",
    [(0, BLACK_BG), (1, RED_BG), (2, GREEN_BG), (3, YELLOW_BG),
     (4, BLUE_BG), (5, MAGENTA_BG), (6, CYAN_BG), (7, WHITE_BG),
     (8, BRIGHT_BLACK_BG), (9, BRIGHT_RED_BG), (10, BRIGHT_GREEN_BG), (11, BRIGHT_YELLOW_BG),
     (12, BRIGHT_BLUE_BG), (13, BRIGHT_MAGENTA_BG), (14, BRIGHT_CYAN_BG), (15, BRIGHT_WHITE_BG)
    ])
def test_color16_background_constants(bg_index, constant):
    expected = color16(bg=bg_index)
    assert constant == expected


@pytest.mark.parametrize(
    "constant, constant_fg",
    [(BLACK, BLACK_FG), (RED, RED_FG), (GREEN, GREEN_FG), (YELLOW, YELLOW_FG),
     (BLUE, BLUE_FG), (MAGENTA, MAGENTA_FG), (CYAN, CYAN_FG), (WHITE, WHITE_FG),
     (BRIGHT_BLACK, BRIGHT_BLACK_FG), (BRIGHT_RED, BRIGHT_RED_FG),
     (BRIGHT_GREEN, BRIGHT_GREEN_FG), (BRIGHT_YELLOW, BRIGHT_YELLOW_FG),
     (BRIGHT_BLUE, BRIGHT_BLUE_FG), (BRIGHT_MAGENTA, BRIGHT_MAGENTA_FG),
     (BRIGHT_CYAN, BRIGHT_CYAN_FG), (BRIGHT_WHITE, BRIGHT_WHITE_FG)
    ])
def test_color16_foreground_constants_equal(constant, constant_fg):
    assert constant == constant_fg
