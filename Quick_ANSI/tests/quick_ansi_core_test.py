import pytest
from ..src.quick_ansi import *
from ..src.quick_ansi import _ESC, _CSI


@pytest.mark.parametrize(
    "actual, expected",
    [(_ESC, '\033'),
     (_CSI, '\033' + '['),
     (RESET, '\033' + '[0m'),
     (CLEAR, '\033' + '[2J' + '\033' + '[1;1H'),
     (CLEAR, CLEAR_SCREEN),
     (color16, c16),
     (color16, color),
     (color256, c256),
     (DEFAULT_COLOR, '\033' + '[39m'),
     (DEFAULT_COLOR_FG, DEFAULT_COLOR),
     (DEFAULT_COLOR_BG, '\033' + '[49m'),
     (DEFAULT_COLORS, DEFAULT_COLOR_FG + DEFAULT_COLOR_BG)
    ])
def test_quick_ansi_core_constants(actual, expected):
    assert actual == expected
