import pytest
from ..src.quick_ansi import *


def test_color256_no_values_provided():
    expected = "No 256-Color Values Provided"
    with pytest.raises(ValueError) as ve:
        color256()
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "fg_index",
     ["a", -1, 256, 3.5])
def test_color256_invalid_foreground_values(fg_index):
    expected = f"Invalid 256-Color Foreground Value [0 - 255]: {fg_index}"
    with pytest.raises(ValueError) as ve:
        color256(fg=fg_index)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "bg_index",
     ["a", -1, 256, 3.5])
def test_color256_invalid_background_values(bg_index):
    expected = f"Invalid 256-Color Background Value [0 - 255]: {bg_index}"
    with pytest.raises(ValueError) as ve:
        color256(bg=bg_index)
    assert expected == str(ve.value)


def test_color256_foreground_color():
    fg_index = 64
    expected = '\033' + "[38;5;" + str(fg_index) + "m"
    actual = color256(fg=fg_index)
    assert actual == expected


def test_color256_background_color():
    bg_index = 128
    expected = '\033' + "[48;5;" + str(bg_index) + "m"
    actual = color256(bg=bg_index)
    assert actual == expected


def test_color256_foreground_plus_background_color():
    fg_index = 32
    bg_index = 192
    expected = color256(fg=fg_index)
    expected += color256(bg=bg_index)
    actual = color256(fg_index, bg_index)
    assert actual == expected
