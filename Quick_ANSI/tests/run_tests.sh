#!/usr/bin/env bash
set -e

TESTS_DIR=$(pwd)
PROJECT_DIR="${TESTS_DIR}/.."
SRC_DIR="${PROJECT_DIR}/src"
CONDA_ENV="py36_env"


# Main Processing Logic
function main() {
    setup
    set_conda_environment
    run_tests
    cleanup
}


# Run unit tests
function run_tests() {
    echo "Running unit tests"
    cd "${TESTS_DIR}"
    set +e
    python -m pytest
    set -e
}


# Setup required to run tests
function setup() {
    # Create __init__.py files
    echo "Setting up __init__.py files"
    touch "${PROJECT_DIR}/__init__.py"
    touch "${SRC_DIR}/__init__.py"
    touch "${TESTS_DIR}/__init__.py"
}


# Switch to the required conda environment
function set_conda_environment() {
    if [ -n "${CONDA_DEFAULT_ENV}" ] && [ "${CONDA_DEFAULT_ENV}" == "${CONDA_ENV}" ]
    then
        echo "Already in Conda Environment: ${CONDA_DEFAULT_ENV}"
    else
        echo "Activating Conda Environment: ${CONDA_ENV}"
        source activate "${CONDA_ENV}"
    fi
}


# Cleanup after tests
function cleanup() {
    echo "Cleaning up files"
    cd "${PROJECT_DIR}"
    # Delete __init__.py files
    echo "Deleting __init__.py files"
    rm "${PROJECT_DIR}/__init__.py"
    rm "${SRC_DIR}/__init__.py"
    rm "${TESTS_DIR}/__init__.py"

    echo "Deleting __pycache__ directories"
    find . -name __pycache__ -type d -print0 | xargs -0 rm -rf
    echo "Deleting .pytest_cache directories"
    find . -name .pytest_cache -type d -print0 | xargs -0 rm -rf
}


main
