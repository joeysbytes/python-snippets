import pytest
from ..src.quick_ansi import *


@pytest.mark.parametrize(
    "actual, expected",
    [(NORMAL, '\033' + '[22m'),
     (BOLD, NORMAL + '\033' + '[1m'),
     (DIM, NORMAL + '\033' + '[2m'),
     (ITALIC, '\033' + '[3m'),
     (ITALIC, ITALIC_ON),
     (ITALIC_OFF, '\033' + '[23m'),
     (UNDERLINE, '\033' + '[4m'),
     (UNDERLINE, UNDERLINE_ON),
     (UNDERLINE_OFF, '\033' + '[24m'),
     (BLINK, '\033' + '[5m'),
     (BLINK, BLINK_ON),
     (BLINK_OFF, '\033' + '[25m'),
     (REVERSE, '\033' + '[7m'),
     (REVERSE, REVERSE_ON),
     (REVERSE_OFF, '\033' + '[27m'),
     (STRIKETHROUGH, '\033' + '[9m'),
     (STRIKETHROUGH, STRIKETHROUGH_ON),
     (STRIKETHROUGH_OFF, '\033' + '[29m'),
     (HIDE, '\033' + '[8m'),
     (SHOW, '\033' + '[28m'),
     (HIDE, HIDE_ON),
     (SHOW, SHOW_ON),
     (HIDE_OFF, SHOW),
     (SHOW_OFF, HIDE)
    ])
def test_attribute_constants(actual, expected):
    assert actual == expected
