import sys
sys.path.append('../src')
from quick_ansi import *
import random

THEME = {"default": rgb((170, 170, 170), (0, 0, 0)),
         "heading": rgb((255, 255, 85), (0, 0, 0)),
         "heading_und": rgb((255, 255, 255), (0, 0, 0)),
         "press_enter": rgb((0, 170, 170), (0, 0, 0)),
         "section_hdg": rgb((85, 85, 255), (0, 0, 0)),
         "line_lbl": rgb((170, 170, 170), (0, 0, 0))
        }
DEFAULT = THEME["default"]


def main():
    setup()
    print_screen_heading()
    prntln()
    print_color_section(1, 0, 0, "Red")
    prntln()
    print_color_section(0, 1, 0, "Green")
    prntln()
    print_color_section(0, 0, 1, "Blue")
    prntln()
    press_enter()
    setup()
    print_screen_heading()
    prntln()
    print_color_section(1, 1, 0, "Red + Green")
    prntln()
    print_color_section(1, 0, 1, "Red + Blue")
    prntln()
    print_color_section(0, 1, 1, "Green + Blue")
    prntln()
    press_enter()
    setup()
    print_screen_heading()
    prntln()
    print_color_section(1, 1, 1, "Red + Green + Blue")
    prntln()
    print_random_colors()
    prntln()
    press_enter()
    cleanup()


def print_screen_heading():
    heading = "RGB Color ANSI Codes"
    prntln(txt(heading, "heading") + DEFAULT)
    prntln(txt("=" * len(heading), "heading_und"))


def print_color_section(red, green, blue, heading):
    prntln(txt(heading, "section_hdg") + DEFAULT)
    for row in range(0, 4):
        start = row * 64
        line = txt("{: >3d}".format(start), "line_lbl")
        line += txt("  ")
        for col in range(start, start + 64):
            line += rgb((170, 170, 0), (red * col, green * col, blue * col))
            if (col +1) % 8 == 0:
                line += ","
            else:
                line += "."
        line += txt("  ")
        line += txt("{: <3d}".format(start + 63), "line_lbl")
        line += DEFAULT
        prntln(line)


def print_random_colors():
    character_list = r"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()`~-_=+[{]}\|;:'\",<.>/?"
    prntln(txt("A Very Colorful Quilt", "section_hdg"))
    for _ in range(0, 10):
        line = ""
        for _ in range(0, 74):
            fg_r = random.randint(0, 255)
            fg_g = random.randint(0, 255)
            fg_b = random.randint(0, 255)
            bg_r = random.randint(0, 255)
            bg_g = random.randint(0, 255)
            bg_b = random.randint(0, 255)
            character = character_list[random.randint(0, len(character_list) - 1)]
            line += rgb((fg_r, fg_g, fg_b), (bg_r, bg_g, bg_b))
            line += character
        line += DEFAULT
        prntln(line)


def press_enter():
    message = "Press ENTER to continue..."
    prnt(txt(message, "press_enter") + DEFAULT)
    input()


def txt(text="", theme="default"):
    # Returns text preceded by the theme key (saves typing)
    return f"{THEME[theme]}{text}"


def setup():
    prnt(txt(CLEAR_SCREEN))


def cleanup():
    prnt(f"{RESET}{CLEAR_SCREEN}")


def prnt(text):
    print(text, end="", flush=True)


def prntln(text=""):
    print(text, flush=True)


if __name__ == "__main__":
    main()
