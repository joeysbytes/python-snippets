import sys
sys.path.append('../src')
from quick_ansi import *

THEME = {"default": color256(251, 16),
         "heading": color256(226, 16),
         "heading_und": color256(255, 16),
         "press_enter": color256(37, 16),
         "section_hdg": color256(63, 16)
        }
DEFAULT = THEME["default"]


def main():
    setup()
    print_screen_heading()
    prntln()
    print_first_16_colors()
    prntln()
    print_next_216_colors()
    prntln()
    print_next_24_colors()
    prntln()
    press_enter()
    cleanup()


def setup():
    prnt(txt(CLEAR_SCREEN))


def print_screen_heading():
    heading = "256-Color ANSI Codes"
    prntln(txt(heading, "heading") + DEFAULT)
    prntln(txt("=" * len(heading), "heading_und"))


def print_first_16_colors():
    prntln(txt("First 16 Standard Colors", "section_hdg") + DEFAULT)
    prntln(txt("  ") + build_number_range(0, 7) + txt(" ") + build_number_range(8, 15))


def print_next_216_colors():
    prntln(txt("216 (6 x 6 x 6) Color Cube", "section_hdg") + DEFAULT)
    for row in range(0, 6):
        prnt(txt("  "))
        for square in range(0, 3):
            start = 16 + (row * 36) + (square * 6)
            prnt(build_number_range(start, start + 5))
            if square < 2:
                prnt(txt(" "))
        prntln()
    prntln()
    for row in range(0, 6):
        prnt(txt("  "))
        for square in range(0, 3):
            start = 34 + (row * 36) + (square * 6)
            prnt(build_number_range(start, start + 5))
            if square < 2:
                prnt(txt(" "))
        prntln()


def print_next_24_colors():
    prntln(txt("24 Grayscale Colors", "section_hdg") + DEFAULT)
    for row in range(0, 2):
        start = 232 + (row * 12)
        prntln(txt("  ") + build_number_range(start, start + 11) + DEFAULT)


def build_number_range(start, end, width=4) -> str:
    text = ""
    for bg in range(start, end + 1):
        if bg in (0, 16, 17, 18, 19, 20, 21, 232, 233, 234, 235, 236, 237):
            fg = 246
        else:
            fg = 16
        text += color256(fg, bg)
        fmt = "{: >" + str(width) + "d}"
        text += fmt.format(bg)
    text += DEFAULT
    return text


def press_enter():
    message = "Press ENTER to continue..."
    prnt(txt(message, "press_enter") + DEFAULT)
    input()


def txt(text="", theme="default"):
    # Returns text preceded by the theme key (saves typing)
    return f"{THEME[theme]}{text}"


def cleanup():
    prnt(f"{RESET}{CLEAR_SCREEN}")


def prnt(text):
    print(text, end="", flush=True)


def prntln(text=""):
    print(text, flush=True)


if __name__ == "__main__":
    main()
