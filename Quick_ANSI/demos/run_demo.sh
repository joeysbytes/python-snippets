#!/usr/bin/env bash
set -e

DEMO_TO_RUN="${1}"
DEMOS_DIR=$(pwd)
PROJECT_DIR="${DEMOS_DIR}/.."
SRC_DIR="${PROJECT_DIR}/src"
CONDA_ENV="py36_env"


# Main Processing Logic
function main() {
    setup
    set_conda_environment
    run_demo
    cleanup
}


function run_demo() {
    local demo_file="${DEMO_TO_RUN}_demo.py"
    if [ -f "${demo_file}" ]
    then
        python "${demo_file}"
    else
        echo "ERROR: Demo file not found: ${demo_file}"
    fi
}


# Setup required to run demo
function setup() {
    # Create __init__.py files
    echo "Setting up __init__.py files"
    touch "${PROJECT_DIR}/__init__.py"
    touch "${SRC_DIR}/__init__.py"
    touch "${DEMOS_DIR}/__init__.py"
}


# Switch to the required conda environment
function set_conda_environment() {
    if [ -n "${CONDA_DEFAULT_ENV}" ] && [ "${CONDA_DEFAULT_ENV}" == "${CONDA_ENV}" ]
    then
        echo "Already in Conda Environment: ${CONDA_DEFAULT_ENV}"
    else
        echo "Activating Conda Environment: ${CONDA_ENV}"
        source activate "${CONDA_ENV}"
    fi
}


# Cleanup after demo
function cleanup() {
    echo "Cleaning up files"
    cd "${PROJECT_DIR}"
    # Delete __init__.py files
    echo "Deleting __init__.py files"
    rm "${PROJECT_DIR}/__init__.py"
    rm "${SRC_DIR}/__init__.py"
    rm "${DEMOS_DIR}/__init__.py"

    # Delete cache directories
    echo "Deleting __pycache__ directories"
    find . -name __pycache__ -type d -print0 | xargs -0 rm -rf
}


main
