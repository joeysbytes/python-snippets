import sys
sys.path.append('../src')
from quick_ansi import *

THEME = {"default": f"{WHITE_FG}{BLACK_BG}",
         "heading": f"{BRIGHT_YELLOW_FG}{BLACK_BG}",
         "heading_und": f"{BRIGHT_WHITE_FG}{BLACK_BG}",
         "press_enter": f"{CYAN_FG}{BLACK_BG}",
         "table_hdg": f"{BRIGHT_BLUE_FG}{BLACK_BG}",
         "table_hdg_und": f"{BRIGHT_GREEN_FG}{BLACK_BG}",
         "table_col_sep": f"{BRIGHT_BLACK_FG}{BLACK_BG}",
         "data_idx": f"{WHITE_FG}{BLACK_BG}"
        }
DEFAULT = THEME["default"]


def main():
    setup()
    print_screen_heading()
    prntln()
    print_table_heading()
    print_table_data()
    prntln()
    press_enter()
    cleanup()


def print_screen_heading():
    heading = "16-Color ANSI Codes"
    prntln(txt(heading, "heading") + DEFAULT)
    prntln(txt("=" * len(heading), "heading_und"))


def print_table_heading():
    # Column Labels
    line_1 = txt("Idx", "table_hdg")
    line_1 += txt("   ")
    line_1 += txt("Bar", "table_hdg")
    line_1 += txt("   ")
    line_1 += txt("Constant", "table_hdg")
    line_1 += txt("  ")
    line_1 += txt("|", "table_col_sep")
    line_1 += txt("  ")
    line_1 += txt("Idx", "table_hdg")
    line_1 += txt("   ")
    line_1 += txt("Bar", "table_hdg")
    line_1 += txt("      ")
    line_1 += txt("Constant", "table_hdg")
    line_1 += txt("   ")
    prntln(line_1)

    # Underlines
    line_2 = txt("---", "table_hdg_und")
    line_2 += txt("  ")
    line_2 += txt("-----", "table_hdg_und")
    line_2 += txt("  ")
    line_2 += txt("--------", "table_hdg_und")
    line_2 += txt("  ")
    line_2 += txt("|", "table_col_sep")
    line_2 += txt("  ")
    line_2 += txt("---", "table_hdg_und")
    line_2 += txt("  ")
    line_2 += txt("-----", "table_hdg_und")
    line_2 += txt("  ")
    line_2 += txt("--------------", "table_hdg_und")
    line_2 += DEFAULT
    prntln(line_2)


def print_table_data():
    colors_left = ["BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE"]
    for idx_left in range(0, 8):
        print_table_data_line(idx_left, colors_left[idx_left])


def print_table_data_line(idx_left, color_left):
    idx_right = idx_left + 8
    color_left_fg = f"{color_left}_FG"
    color_left_bg = f"{color_left}_BG"
    color_right = f"BRIGHT_{color_left}"
    color_right_fg = f"{color_right}_FG"
    color_right_bg = f"{color_right}_BG"

    # left index number
    idx_left_txt = "{: >3d}".format(idx_left)
    line = txt(idx_left_txt, "data_idx")
    line += txt("  ")

    # Color Bar: Use both constant name and color index
    line += globals()[color_left_bg] + "  "
    line += color16(bg=idx_left) + "   "
    line += txt("  ")

    # Color Name: Use both constant names and color index
    if idx_left == 0:
        line += BRIGHT_BLACK_BG
    line += globals()[color_left] + color_left[0:1]
    line += globals()[color_left_fg] + color_left[1:2]
    line += color16(fg=idx_left) + "{: <6}".format(color_left[2:])
    line += txt("  ")

    # Column Separator
    line += txt("|", "table_col_sep")
    line += txt("  ")

    # right index number
    idx_right_txt = "{: >3d}".format(idx_right)
    line += txt(idx_right_txt, "data_idx")
    line += txt("  ")

    # Color Bar: Use both constant name and color index
    line += globals()[color_right_bg] + "  "
    line += color16(bg=idx_right) + "   "
    line += txt("  ")

    # Color Name: Use both constant names and color index
    line += globals()[color_right] + color_right[0:1]
    line += globals()[color_right_fg] + color_right[1:2]
    line += color16(fg=idx_right) + "{: <6}".format(color_right[2:])
    line += txt()

    prntln(line)


def press_enter():
    message = "Press ENTER to continue..."
    prnt(txt(message, "press_enter") + DEFAULT)
    input()


def txt(text="", theme="default"):
    # Returns text preceded by the theme key (saves typing)
    return f"{THEME[theme]}{text}"


def setup():
    prnt(txt(CLEAR_SCREEN))


def cleanup():
    prnt(f"{RESET}{CLEAR_SCREEN}")


def prnt(text):
    print(text, end="", flush=True)


def prntln(text=""):
    print(text, flush=True)


if __name__ == "__main__":
    main()
