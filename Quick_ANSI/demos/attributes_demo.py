import sys
sys.path.append('../src')
from quick_ansi import *

THEME = {"default": f"{WHITE_FG}{BLACK_BG}",
         "heading": f"{BRIGHT_YELLOW_FG}{BLACK_BG}",
         "heading_und": f"{BRIGHT_WHITE_FG}{BLACK_BG}",
         "press_enter": f"{CYAN_FG}{BLACK_BG}",
         "line_lbl": f"{BRIGHT_BLUE_FG}{BLACK_BG}"
        }
DEFAULT = THEME["default"]


def main():
    setup()
    print_screen_heading()
    prntln()
    print_attribute_lines()
    prntln()
    press_enter()
    cleanup()


def print_screen_heading():
    heading = "Attributes ANSI Codes"
    prntln(txt(heading, "heading") + DEFAULT)
    prntln(txt("=" * len(heading), "heading_und"))


def print_attribute_lines():
    col_1_fmt = "{: <15}"

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Intensities:") + txt("  ")
    line += f"{DIM}DIM{NORMAL}  NORMAL  {BOLD}BOLD{NORMAL}"
    prntln(line)
    prntln()

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Italics:") + txt("  ")
    line += f"{ITALIC}ITALIC{ITALIC_OFF}  ITALIC_OFF  {ITALIC_ON}ITALIC_ON{ITALIC_OFF}"
    prntln(line)
    prntln()

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Underline:") + txt("  ")
    line += f"{UNDERLINE}UNDERLINE{UNDERLINE_OFF}  UNDERLINE_OFF  {UNDERLINE_ON}UNDERLINE_ON{UNDERLINE_OFF}"
    prntln(line)
    prntln()

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Blink:") + txt("  ")
    line += f"{BLINK}BLINK{BLINK_OFF}  BLINK_OFF  {BLINK_ON}BLINK_ON{BLINK_OFF}"
    prntln(line)
    prntln()

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Reverse:") + txt("  ")
    line += f"{REVERSE}REVERSE{REVERSE_OFF}  REVERSE_OFF  {REVERSE_ON}REVERSE_ON{REVERSE_OFF}"
    prntln(line)
    prntln()

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Strikethrough:") + txt("  ")
    line += f"{STRIKETHROUGH}STRIKETHROUGH{STRIKETHROUGH_OFF}  STRIKETHROUGH_OFF  {STRIKETHROUGH_ON}STRIKETHROUGH_ON{STRIKETHROUGH_OFF}"
    prntln(line)
    prntln()

    line = THEME["line_lbl"]
    line += col_1_fmt.format("Hide/Show:") + txt("  ")
    line += f"{HIDE}HIDE{SHOW}  SHOW  {HIDE_ON}HIDE_ON{HIDE_OFF}  HIDE_OFF  {SHOW_OFF}SHOW_OFF{SHOW_ON}  SHOW_ON"
    prntln(line)


def press_enter():
    message = "Press ENTER to continue..."
    prnt(txt(message, "press_enter") + DEFAULT)
    input()


def txt(text="", theme="default"):
    # Returns text preceded by the theme key (saves typing)
    return f"{THEME[theme]}{text}"


def setup():
    prnt(txt(CLEAR_SCREEN))


def cleanup():
    prnt(f"{RESET}{CLEAR_SCREEN}")


def prnt(text):
    print(text, end="", flush=True)


def prntln(text=""):
    print(text, flush=True)


if __name__ == "__main__":
    main()
