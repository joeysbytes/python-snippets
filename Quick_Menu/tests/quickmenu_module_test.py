import pytest
from ..src.QuickMenu import _DEFAULT_COLORS, _NEWLINE, _color16


@pytest.mark.parametrize(
    "actual, expected",
    [(_DEFAULT_COLORS, _color16(-1, -1)),
     (_NEWLINE, '\n')
    ])
def test_module_constants(actual, expected):
    assert actual == expected
