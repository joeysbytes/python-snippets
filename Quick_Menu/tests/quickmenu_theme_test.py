import pytest
from ..src.QuickMenu import QuickMenu
# TODO: Test Theme Defaults when they are finalized

menu = None

def setup_module():
    global menu
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)


###########################################################################
# Theme Tests
###########################################################################

def test_theme_equals_default():
    expected = QuickMenu._DEFAULT_THEME
    actual = menu.theme
    assert actual == expected


INVALID_THEME_KEYS = ["abc", 123, 3.14, None]
INVALID_THEME_VALUES = ["a", 3.14]


@pytest.mark.parametrize(
    "key", INVALID_THEME_KEYS)
def test_get_theme_property_invalid_key(key):
    expected = "Invalid Theme Key: " + str(key) + ", valid keys are: " + str(QuickMenu._THEME_KEYS)
    with pytest.raises(ValueError) as ve:
        menu.get_theme_property(key)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "key", INVALID_THEME_KEYS)
def test_set_theme_property_invalid_key(key):
    expected = "Invalid Theme Key: " + str(key) + ", valid keys are: " + str(QuickMenu._THEME_KEYS)
    with pytest.raises(ValueError) as ve:
        menu.set_theme_property(key)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "fg_idx", INVALID_THEME_VALUES)
def test_set_theme_property_invalid_foreground(fg_idx):
    key = QuickMenu._THEME_KEYS[0]
    expected = "Invalid foreground value: " + str(fg_idx)
    with pytest.raises(ValueError) as ve:
        menu.set_theme_property(key, fg=fg_idx)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "bg_idx", INVALID_THEME_VALUES)
def test_set_theme_property_invalid_background(bg_idx):
    key = QuickMenu._THEME_KEYS[0]
    expected = "Invalid background value: " + str(bg_idx)
    with pytest.raises(ValueError) as ve:
        menu.set_theme_property(key, bg=bg_idx)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "fg_idx, bg_idx, expected",
    [(None, None, (-1, -1)), (5, None, (5, -1)), (None, 13, (-1, 13)), (16, 16, (-1, -1)),
     (-5, -6, (-1, -1)), (-1, -1, (-1, -1)), (4, 12, (4, 12))
    ])
def test_set_theme_property_valid_values(fg_idx, bg_idx, expected):
    key = QuickMenu._THEME_KEYS[0]
    menu.set_theme_property(key, fg=fg_idx, bg=bg_idx)
    assert menu.get_theme_property(key) == expected
