import pytest
from ..src.QuickMenu import QuickMenu

###########################################################################
# Default Values
###########################################################################

@pytest.mark.parametrize(
    "property_name, expected",
    [("color_enabled", False),
     ("_input_error", False),
     ("_menu_cache", None),
     ("_reload_menu_cache", True),
     ("title", None),
     ("title_underline", "-"),
     ("pre_menu", None),
     ("items", ("Item 1", "Item 2", "Item 3")),
     ("num_items", 3),
     ("item_separator", ")"),
     ("post_menu", None),
     ("prompt", "Enter Choice"),
     ("prompt_separator", ":")
    ])
def test_default_properties(property_name, expected):
    menu_items = ["Item 1", "Item 2", "Item 3"]
    menu = QuickMenu(menu_items)
    actual = getattr(menu, property_name)
    assert actual == expected


def test_default_properties_2():
    menu_items = ["Item 1", "Item 2", "Item 3"]
    menu = QuickMenu(menu_items)
    assert isinstance(menu.items, tuple)


###########################################################################
# Title
###########################################################################

@pytest.mark.parametrize(
    "title_text, expected",
    [("Main Menu", "Main Menu"), (123, "123"), (None, None), ("", None)
    ])
def test_title_property(title_text, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, title=title_text)
    assert menu.title == expected


def test_title_setter_updates_reload_menu_cache_flag():
    title = "The Super Menu"
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.title = title
    assert menu._reload_menu_cache == True


@pytest.mark.parametrize(
    "underline_char, expected",
    [("=", "="), (12, "12"), (None, None), ("", None)
    ])
def test_title_underline_property(underline_char, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, title_underline=underline_char)
    assert menu.title_underline == expected


def test_title_underline_setter_updates_reload_menu_cache_flag():
    underline_char = "="
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.title_underline = underline_char
    assert menu._reload_menu_cache == True


###########################################################################
# Pre-Menu Text
###########################################################################

@pytest.mark.parametrize(
    "pre_menu_text, expected",
    [("Text Before Menu", "Text Before Menu"), (123, "123"), (None, None), ("", None)
    ])
def test_pre_menu_property(pre_menu_text, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, pre_menu=pre_menu_text)
    assert menu.pre_menu == expected


def test_pre_menu_setter_updates_reload_menu_cache_flag():
    pre_menu = "This text appears before the menu items."
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.pre_menu = pre_menu
    assert menu._reload_menu_cache == True


###########################################################################
# Post-Menu Text
###########################################################################

@pytest.mark.parametrize(
    "post_menu_text, expected",
    [("Text After Menu", "Text After Menu"), (123, "123"), (None, None), ("", None)
    ])
def test_post_menu_property(post_menu_text, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, post_menu=post_menu_text)
    assert menu.post_menu == expected


def test_post_menu_setter_updates_reload_menu_cache_flag():
    post_menu = "This text appears after the menu items."
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.post_menu = post_menu
    assert menu._reload_menu_cache == True


###########################################################################
# Prompt
###########################################################################

@pytest.mark.parametrize(
    "prompt_text, expected",
    [("Make a Choice", "Make a Choice"), (123, "123"), (None, None), ("", None)
    ])
def test_prompt_property(prompt_text, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, prompt=prompt_text)
    assert menu.prompt == expected


def test_prompt_setter_updates_reload_menu_cache_flag():
    prompt = "Enter Something Already"
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.prompt = prompt
    assert menu._reload_menu_cache == True


@pytest.mark.parametrize(
    "prompt_separator_char, expected",
    [(">", ">"), (123, "123"), (None, None), ("", None)
    ])
def test_prompt_separator_property(prompt_separator_char, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, prompt_separator=prompt_separator_char)
    assert menu.prompt_separator == expected


def test_prompt_separator_setter_updates_reload_menu_cache_flag():
    prompt_separator = ">"
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.prompt_separator = prompt_separator
    assert menu._reload_menu_cache == True


###########################################################################
# Color Enabled
###########################################################################

@pytest.mark.parametrize(
    "is_color_enabled", [True, False])
def test_color_enabled_property(is_color_enabled):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, color_enabled=is_color_enabled)
    assert menu.color_enabled == is_color_enabled


@pytest.mark.parametrize(
    "is_color_enabled", ["a", 0, 1, 0.5])
def test_color_enabled_property_invalid_values(is_color_enabled):
    key = "color_enabled"
    expected = "Boolean value required for property '" + key + "', received: " + str(is_color_enabled)
    menu_items = ("Item 1", "Item 2")
    with pytest.raises(ValueError) as ve:
        menu = QuickMenu(menu_items, color_enabled=is_color_enabled)
    assert expected == str(ve.value)


def test_color_enabled_setter_updates_reload_menu_cache_flag():
    color_enabled = True
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.color_enabled = color_enabled
    assert menu._reload_menu_cache == True


###########################################################################
# Menu Items
###########################################################################

@pytest.mark.parametrize(
    "items",
    [("Item 1", "Item 2", "Item 3", "Item 4", "Item 5"),
     ["New Item 1", "New Item 2", 123, "New Item 4", 3.1415, None, "New Item 7"]
    ])
def test_menu_items_property(items):
    expected_list = []
    for item in items:
        expected_list.append(str(item))
    expected = tuple(expected_list)
    menu = QuickMenu(items)
    assert menu.items == expected
    assert menu.num_items == len(items)


@pytest.mark.parametrize(
    "items", [123, None, 3.14, {"key1": "item1", "key2": "item2"}])
def test_menu_items_property_invalid_values(items):
    expected = "Menu items are not a sequence, received: " + str(items)
    with pytest.raises(ValueError) as ve:
        menu = QuickMenu(items)
    assert expected == str(ve.value)


@pytest.mark.parametrize(
    "item_separator_char, expected",
    [("]", "]"), (123, "123"), (None, None), ("", None)
    ])
def test_item_separator_property(item_separator_char, expected):
    menu_items = ("Item 1", "Item 2")
    menu = QuickMenu(menu_items, item_separator=item_separator_char)
    assert menu.item_separator == expected


def test_menu_items_setter_updates_reload_menu_cache_flag():
    menu_items = ("Item 1", "Item 2")
    menu_items_2 = ("Item 3", "Item 4")
    menu = QuickMenu(menu_items)
    menu._reload_menu_cache = False
    menu.items = menu_items_2
    assert menu._reload_menu_cache == True
