from typing import Any, Optional, Sequence, Tuple
from copy import deepcopy

# TODO: __str__
# TODO: Demo (calculator?)
# TODO: Build Menu
# TODO: Get User Choice
# TODO: Documentation

def _color16(fg: int=None,  bg: int=None) -> str:
    code = ""
    if (fg is not None) and (isinstance(fg, int)):
        fg_code = 30 + fg if 0 <= fg <= 7 else 82 + fg if 8 <= fg <= 15 else 39; code += f"\033[{fg_code}m"
    if (bg is not None) and (isinstance(bg, int)):
        bg_code = 40 + bg if 0 <= bg <= 7 else 92 + bg if 8 <= bg <= 15 else 49; code += f"\033[{bg_code}m"
    return code

_NEWLINE = '\n'
_DEFAULT_COLORS = _color16(-1, -1)


class QuickMenu:

    _DEFAULT_THEME = {"title": (15, -1),
                      "title_underline": (15, -1),
                      "pre_menu": (7, -1),
                      "item_num": (6, -1),
                      "item_separator": (5, -1),
                      "item": (7, -1),
                      "post_menu": (7, -1),
                      "error": (12, 1),
                      "prompt": (6, -1),
                      "prompt_separator": (5, -1),
                      "input": (-1, -1)
                     }
    _THEME_KEYS = tuple(_DEFAULT_THEME)


    def __init__(self, items: Sequence, title: Optional[str]=None, title_underline: Optional[str]="-",
                 pre_menu: Optional[str]=None, post_menu: Optional[str]=None,
                 prompt: Optional[str]="Enter Choice", color_enabled: bool=False,
                 item_separator: Optional[str]=")", prompt_separator: Optional[str]=":"):
        self._properties = {}
        self._menu_cache = None
        self._reload_menu_cache = True
        self.items = items
        self.item_separator = item_separator
        self.title = title
        self.title_underline = title_underline
        self.pre_menu = pre_menu
        self.post_menu = post_menu
        self.prompt = prompt
        self.prompt_separator = prompt_separator
        self.color_enabled = color_enabled
        self._input_error = False
        self._theme = deepcopy(QuickMenu._DEFAULT_THEME)



    #######################################################################
    # Menu Items
    #######################################################################

    @property
    def items(self) -> Tuple[str]:
        """Return a tuple of the menu items."""
        return deepcopy(self._items)

    @items.setter
    def items(self, items: Sequence[str]):
        """Give a sequence list of items, convert all to a string and make a tuple of it."""
        if isinstance(items, Sequence):
            items_list = [str(item) for item in items]
            self._items = tuple(items_list)
            self._reload_menu_cache = True
        else:
            raise ValueError(f"Menu items are not a sequence, received: {items}")

    @property
    def num_items(self) -> int:
        """Return the number of menu items."""
        return len(self._items)

    #######################################################################
    # Properties
    #######################################################################

    def _set_property(self, value_type: str, key: str, value: Any, field: str):
        """Utility method to store property values."""
        if value_type == "str":
            if value is None:
                self._properties[key] = None
            else:
                value_str = str(value)
                self._properties[key] = None if len(value_str) == 0 else value_str
        elif value_type == "bool":
            if isinstance(value, bool):
                self._properties[key] = value
            else:
                raise ValueError(f"Boolean value required for property '{key}', received: {value}")
        self._reload_menu_cache = True

    # Menu Title Text

    @property
    def title(self) -> str:
        """Return the title of the menu."""
        return self._properties["title"]

    @title.setter
    def title(self, title_text: str=None):
        """Set the title of the menu."""
        self._set_property("str", "title", title_text, "Menu Title")

    # Menu Title Underline

    @property
    def title_underline(self) -> str:
        """Return the character(s) that make up the underline of the title."""
        return self._properties["title_underline"]

    @title_underline.setter
    def title_underline(self, title_underline_char: str=None):
        """Set the character(s) that make up the underline of the title."""
        self._set_property("str", "title_underline", title_underline_char, "Menu Title Underline Character(s)")

    # Pre-Menu Items Text

    @property
    def pre_menu(self) -> str:
        """Return the text that is printed before the menu items."""
        return self._properties["pre_menu"]

    @pre_menu.setter
    def pre_menu(self, pre_menu_text: str=None):
        """Set the text that is printed before the menu items."""
        self._set_property("str", "pre_menu", pre_menu_text, "Pre-Menu Items Text")

    # Menu Items

    @property
    def item_separator(self) -> str:
        """Return the character(s) that separate the menu item number from the menu item text."""
        return self._properties["item_separator"]

    @item_separator.setter
    def item_separator(self, item_separator_char: str):
        self._set_property("str", "item_separator", item_separator_char, "Menu Item Separator Character(s)")

    # Post-Menu Items Text

    @property
    def post_menu(self) -> str:
        """Return the text that is printed after the menu items."""
        return self._properties["post_menu"]

    @post_menu.setter
    def post_menu(self, post_menu_text: str=None):
        """Set the text that is printed after the menu items."""
        self._set_property("str", "post_menu", post_menu_text, "Post-Menu Items Text")

    # User Prompt

    @property
    def prompt(self) -> str:
        """Return the text used to prompt the user to choose a menu item."""
        return self._properties["prompt"]

    @prompt.setter
    def prompt(self, prompt_text: str=None):
        """Set the text used to prompt the user to choose a menu item."""
        self._set_property("str", "prompt", prompt_text, "User Prompt Text")

    @property
    def prompt_separator(self) -> str:
        """Return the text used to separate the prompt text from the prompt cursor."""
        return self._properties["prompt_separator"]

    @prompt_separator.setter
    def prompt_separator(self, prompt_separator_char: str):
        """Set the text used to separate the prompt text from the prompt cursor."""
        self._set_property("str", "prompt_separator", prompt_separator_char, "User Prompt Separator Character(s)")

    # Color Control

    @property
    def color_enabled(self) -> bool:
        """Return whether to use ANSI color codes on the terminal."""
        return self._properties["color_enabled"]

    @color_enabled.setter
    def color_enabled(self, is_color_enabled: bool):
        """Sets whether to use ANSI color codes on the terminal."""
        self._set_property("bool", "color_enabled", is_color_enabled, "Color Enabled Flag")

    #######################################################################
    # Theme
    #######################################################################

    @property
    def theme(self) -> dict:
        """Return a copy of the theme dictionary."""
        return deepcopy(self._theme)

    def set_theme_property(self, key: str, fg: Optional[int]=-1, bg: Optional[int]=-1):
        """Set a theme property to the provided foreground and background color indexes.
        Any number on in the range of 0 - 15 will result in the ANSI code for the terminal
        default color."""
        if key not in QuickMenu._THEME_KEYS:
            msg = f"Invalid Theme Key: {key}, valid keys are: {QuickMenu._THEME_KEYS}"
            raise ValueError(msg)
        fg_idx = -1
        bg_idx = -1
        if fg is not None:
            if isinstance(fg, int):
                if 0 <= fg <= 15:
                    fg_idx = fg
            else:
                raise ValueError(f"Invalid foreground value: {fg}")
        if bg is not None:
            if isinstance(bg, int):
                if 0 <= bg <= 15:
                    bg_idx = bg
            else:
                raise ValueError(f"Invalid background value: {bg}")
        self._theme[key] = (fg_idx, bg_idx)
        self._reload_menu_cache = True

    def get_theme_property(self, key: str) -> Tuple[int, int]:
        """Returns the theme tuple (foreground, background) of a given key."""
        if key not in QuickMenu._THEME_KEYS:
            msg = f"Invalid Theme Key: {key}, valid keys are: {QuickMenu._THEME_KEYS}"
            raise ValueError(msg)
        return self._theme[key]

    #######################################################################
    # Build Menu Text
    #######################################################################

    def _build_menu_cache(self):
        """Build the full menu text and store it in the menu cache."""
        if self._reload_menu_cache:
            self._menu_cache = ""
            self._menu_cache += self._build_menu_title()
            self._reload_menu_cache = False

    def _build_menu_title(self) -> str:
        """Build the menu title with optional underline."""
        title = ""
        if self.title is not None:
            color = _color16
            newline = _NEWLINE
            default_colors = _DEFAULT_COLORS
            # Title Text
            if self.color_enabled:
                title += color(self.get_theme_property("title"))
            title += self.title
            if self.color_enabled:
                title += default_colors
            title += newline
            # Title Underline
            if self.title_underline is not None:
                if self.color_enabled:
                    title += color(self.get_theme_property("title_underline"))
                underline = self.title_underline * len(self.title)
                title += underline[0:len(self.title)]
                if self.color_enabled:
                    title += default_colors
                title += newline
            title += newline
        return title

    #######################################################################
    # Prompt User For Choice
    #######################################################################

    def get_user_choice(self) -> Tuple[int, str]:
        pass

    def get_user_choice_index() -> int:
        return self.get_user_choice[0]

    def get_user_choice_num() -> int:
        return self.get_user_choice[0] + 1

    def get_user_choice_text() -> str:
        return self.get_user_choice[1]
